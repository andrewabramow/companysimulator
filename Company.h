#pragma once
#include <iostream>

class Task {
private:
	char type;
public:
	Task():type('Z'){}
	Task(char inType) :type(inType) {}
	~Task() {
		std::cout << "Task deleted\n";
	}

	//void SetType(const char& inType);
	const char& GetType() const;
};

class Worker {
private:
	Task* task;
public:
	Worker() {
		task = nullptr;
	}
	Worker(Task* inTask) {
		task = inTask;
	}
	~Worker() {
		std::cout << "Worker fired\n";
	}
	void SetTask(Task* inTask);
	Task* GetTask();
	
};
class Manager {
private:
	int id = 0;
	int seed = 0;
	int workersNumber = 0;
	Task** tasks = nullptr;
	Worker** workers = nullptr;
public:
	Manager(int inId, int inSeed, int inWorkNumb):
		id(inId), seed(inSeed), workersNumber(inWorkNumb){
		
		std::srand(inSeed + inId);
		int tasksNumber = rand() % inWorkNumb;
		if (tasksNumber == 0) ++tasksNumber; // there can be no zero tasks
		tasks = new Task * [tasksNumber];
		workers = new Worker * [inWorkNumb];
		// task type initializaation
		for (int i = 0; i < tasksNumber; ++i) {
			tasks[i] = new Task('A' + rand() % 3);
		}
		// workers initializaation
		for (int j = 0; j < inWorkNumb; ++j) {
			workers[j] = new Worker();
		}
		// assign ALL tasks to random employees
		for (int k = 0; k < tasksNumber; ++k) {
			workers[rand()%inWorkNumb]->SetTask(tasks[k]);
		}
		// assign tasks to REMAINING employees
		for (int m = 0; m < inWorkNumb; ++m) {
			if (workers[m]->GetTask() == nullptr) {
				workers[m]->SetTask(tasks[rand() % tasksNumber]);
			}
		}
	}
	~Manager() {
		std::cout << "Job done\n";
	}
	Worker* GetWorkerAt(const int& n);
	const int& GetWorkerNumb() const;
};